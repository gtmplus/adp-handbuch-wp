<?php

class ThemeSettingsClass {
	const SCRIPTS_VERSION = '1.0.5';

	public function __construct(){
		$this->scriptsDir = get_theme_file_uri().'/assets/js';
        $this->stylesDir = get_theme_file_uri().'/assets/css';

		$this->actions_init();
	}

	public function actions_init(){
		add_action( 'wp_enqueue_scripts', array( $this, 'scripts_styles' ) );
		add_action( 'after_setup_theme', array( $this, 'theme_setup' ) );
		add_action( 'wp_enqueue_scripts',  array( $this, 'js_variables' ) );
		add_action( 'widgets_init', array( $this, 'widgets_init') );
		add_action( 'wp_head', array( $this, 'google_tag_manager') );
		// add_action( 'wp_head', array( $this, 'schema_org') );
		add_action( 'wp_body_open', array( $this, 'google_tag_manager_noscript') );
		add_action( 'init', array( $this, 'custom_post_type') );
		add_action( 'init', array( $this, 'custom_taxonomy') );

		add_filter( 'upload_mimes', array( $this, 'enable_svg_types' ), 99 );
		add_filter( 'wpseo_json_ld_output', '__return_false' );
		add_filter( 'use_block_editor_for_post', '__return_false', 10 );
		add_filter( 'script_loader_tag', array( $this, 'add_async_attribute' ), 10, 2 );
	}

	public function scripts_styles() {
		wp_enqueue_style( 'swiper-css', 'https://unpkg.com/swiper/swiper-bundle.min.css' , '', self::SCRIPTS_VERSION);
		wp_enqueue_style( 'adp-css', $this->stylesDir.'/main.min.css' , '', self::SCRIPTS_VERSION);
    	wp_enqueue_style( 'adp-style', get_stylesheet_uri() );

    	wp_enqueue_script( 'swiper-js', 'https://unpkg.com/swiper/swiper-bundle.min.js', array( 'jquery' ), self::SCRIPTS_VERSION, true );
    	wp_enqueue_script( 'all-js', $this->scriptsDir.'/all.min.js', array( 'jquery' ), self::SCRIPTS_VERSION, true );
    }

    public function theme_setup(){
    	// if( ENV == 'dev' ) update_option( 'upload_url_path', 'https://stage.bcareagency.com/wp-content/uploads', true );

    	load_theme_textdomain( 'adp' );
	    add_theme_support( 'automatic-feed-links' );
	    add_theme_support( 'title-tag' );
	    add_theme_support( 'post-thumbnails' );

	    // add_image_size( 'exrtise-thumbnail', 270, 180, true );
	    
	    register_nav_menus( array(
	        'top'          	=> __( 'Top Menu', 'adp' ),
	        'bottom'        => __( 'Bottom Menu', 'adp' )
	    ) );

	    if( function_exists('acf_add_options_page') ) {
		    $general = acf_add_options_page(array(
		        'page_title'    => __('Theme General Settings', 'adp'),
		        'menu_title'    => __('Theme Settings', 'adp'),
		        'redirect'      => false,
		        'capability'    => 'edit_posts',
		        'menu_slug'     => 'theme-settings',
		    ));
		}

		add_post_type_support( 'page', 'excerpt' );
    }

    public function widgets_init(){
    	register_sidebar( array(
	        'name'          => __( 'Footer 1 (company information)', 'adp' ),
	        'id'            => 'footer-1',
	        'description'   => __( 'Add widgets here to appear in your footer.', 'adp' ),
	        'before_widget' => '<div id="%1$s" class="widget %2$s about__widget">',
	        'after_widget'  => '</div>',
	        'before_title'  => '<h3><b>',
	        'after_title'   => '</b></h3>',
	    ) );
	    register_sidebar( array(
	        'name'          => __( 'Footer 2 (horizontal menu)', 'adp' ),
	        'id'            => 'footer-2',
	        'description'   => __( 'Add widgets here to appear in your footer.', 'adp' ),
	        'before_widget' => '<div id="%1$s" class="widget %2$s">',
	        'after_widget'  => '</div>',
	        'before_title'  => '<h5>',
	        'after_title'   => '</h5>',
	    ) );
	    register_sidebar( array(
	        'name'          => __( 'Footer 3 (vertical menu)', 'adp' ),
	        'id'            => 'footer-3',
	        'description'   => __( 'Add widgets here to appear in your footer.', 'adp' ),
	        'before_widget' => '<div id="%1$s" class="widget %2$s">',
	        'after_widget'  => '</div>',
	        'before_title'  => '<h5>',
	        'after_title'   => '</h5>',
	    ) );
	    register_sidebar( array(
	        'name'          => __( 'Footer 4 (vertical menu)', 'adp' ),
	        'id'            => 'footer-4',
	        'description'   => __( 'Add widgets here to appear in your footer.', 'adp' ),
	        'before_widget' => '<div id="%1$s" class="widget %2$s">',
	        'after_widget'  => '</div>',
	        'before_title'  => '<h5>',
	        'after_title'   => '</h5>',
	    ) );
	    register_sidebar( array(
	        'name'          => __( 'Footer 5 (vertical menu)', 'adp' ),
	        'id'            => 'footer-5',
	        'description'   => __( 'Add widgets here to appear in your footer.', 'adp' ),
	        'before_widget' => '<div id="%1$s" class="widget %2$s">',
	        'after_widget'  => '</div>',
	        'before_title'  => '<h5>',
	        'after_title'   => '</h5>',
	    ) );
    }

    public function enable_svg_types($mimes) {
		$mimes['svg'] = 'image/svg+xml';
		$mimes['svgz'] = 'image/svg+xml';
		return $mimes;
	}

	public function js_variables(){ ?>
		<script type="text/javascript">
	        var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
	    </script>
	<?php }

	public function google_tag_manager() { 
    	if ( ENV == 'prd' ) {
    		get_template_part( 'inc/analytics/gtm' );
    	}
    }

    public function schema_org() { 
    	if ( ENV == 'prd' ) {
    		get_template_part( 'inc/analytics/schema' );
    	}
    }

    public function google_tag_manager_noscript() { 
    	if ( ENV == 'prd' ) {
    		get_template_part( 'inc/analytics/gtm', 'noscript' );
    	}
    }

    public function __return_false() {
        return false;
    }

	public function add_async_attribute($tag, $handle){
	    if(!is_admin()){
	        if ('jquery-core' == $handle) {
	            return $tag;
	        }
	        return str_replace(' src', ' defer src', $tag);
	    } else {
	        return $tag;
	    }
	}

	public function custom_post_type(){
		$post_labels = array(
			'name'					=> __('Persons', 'adp'),
			'singular_name'			=> __('Persons', 'adp'),
			'add_new'				=> __('Add Person', 'adp'),
			'add_new_item'			=> __('Add New Person', 'adp'),
			'edit_item'				=> __('Edit Person', 'adp'),
			'new_item'				=> __('New Person', 'adp'),
			'view_item'				=> __('View Person', 'adp')
		);

		$post_args = array(
			'label'               	=> __('Persons', 'adp'),
			'description'        	=> __('Person information page', 'adp'),
			'labels'              	=> $post_labels,
			'supports'            	=> array( 'title', 'excerpt'),
			'taxonomies'          	=> array( '' ),
			'hierarchical'       	=> false,
			'public'              	=> true,
			'show_ui'             	=> true,
			'show_in_menu'        	=> true,
			'has_archive'         	=> true,
			'can_export'          	=> true,
			'show_in_nav_menus'   	=> true,
			'publicly_queryable'  	=> true,
			'exclude_from_search' 	=> false,
			'query_var'           	=> true,
			'capability_type'     	=> 'post',
			'menu_position'			=> 3,
			'rewrite'				=> array(
				'slug'				=> 'persons'
			),
			'menu_icon'           	=> 'dashicons-buddicons-buddypress-logo'
		);
		register_post_type( 'person', $post_args );

		$post_labels = array(
			'name'					=> __('Institutions', 'adp'),
			'singular_name'			=> __('Institutions', 'adp'),
			'add_new'				=> __('Add Institution', 'adp'),
			'add_new_item'			=> __('Add New Institution', 'adp'),
			'edit_item'				=> __('Edit Institution', 'adp'),
			'new_item'				=> __('New Institution', 'adp'),
			'view_item'				=> __('View Institution', 'adp')
		);

		$post_args = array(
			'label'               	=> __('Institutions', 'adp'),
			'description'        	=> __('Institution information page', 'adp'),
			'labels'              	=> $post_labels,
			'supports'            	=> array( 'title', 'excerpt'),
			'taxonomies'          	=> array( '' ),
			'hierarchical'       	=> false,
			'public'              	=> true,
			'show_ui'             	=> true,
			'show_in_menu'        	=> true,
			'has_archive'         	=> false,
			'can_export'          	=> true,
			'show_in_nav_menus'   	=> true,
			'publicly_queryable'  	=> true,
			'exclude_from_search' 	=> false,
			'query_var'           	=> true,
			'capability_type'     	=> 'post',
			'menu_position'			=> 3,
			'rewrite'				=> array(
				'slug'				=> 'institutions'
			),
			'menu_icon'           	=> 'dashicons-building'
		);
		register_post_type( 'institution', $post_args );

		$post_labels = array(
			'name'					=> __('Regional Groups', 'adp'),
			'singular_name'			=> __('Regional Group', 'adp'),
			'add_new'				=> __('Add Regional Group', 'adp'),
			'add_new_item'			=> __('Add New Regional Group', 'adp'),
			'edit_item'				=> __('Edit Regional Group', 'adp'),
			'new_item'				=> __('New Regional Group', 'adp'),
			'view_item'				=> __('View Regional Group', 'adp')
		);

		$post_args = array(
			'label'               	=> __('Regional Groups', 'adp'),
			'description'        	=> __('Regional Group information page', 'adp'),
			'labels'              	=> $post_labels,
			'supports'            	=> array( 'title', 'excerpt'),
			'taxonomies'          	=> array( '' ),
			'hierarchical'       	=> false,
			'public'              	=> true,
			'show_ui'             	=> true,
			'show_in_menu'        	=> true,
			'has_archive'         	=> false,
			'can_export'          	=> true,
			'show_in_nav_menus'   	=> true,
			'publicly_queryable'  	=> false,
			'exclude_from_search' 	=> false,
			'query_var'           	=> false,
			'capability_type'     	=> 'post',
			'menu_position'			=> 3,
			'rewrite'				=> array(
				'slug'				=> 'regional-groups'
			),
			'menu_icon'           	=> 'dashicons-image-filter'
		);
		register_post_type( 'regional-group', $post_args );

		$post_labels = array(
			'name'					=> __('Books', 'adp'),
			'singular_name'			=> __('Books', 'adp'),
			'add_new'				=> __('Add Book', 'adp'),
			'add_new_item'			=> __('Add New Book', 'adp'),
			'edit_item'				=> __('Edit Book', 'adp'),
			'new_item'				=> __('New Book', 'adp'),
			'view_item'				=> __('View Book', 'adp')
		);

		$post_args = array(
			'label'               	=> __('Books', 'adp'),
			'description'        	=> __('Book information page', 'adp'),
			'labels'              	=> $post_labels,
			'supports'            	=> array( 'title', 'excerpt'),
			'taxonomies'          	=> array( '' ),
			'hierarchical'       	=> false,
			'public'              	=> true,
			'show_ui'             	=> true,
			'show_in_menu'        	=> true,
			'has_archive'         	=> true,
			'can_export'          	=> true,
			'show_in_nav_menus'   	=> true,
			'publicly_queryable'  	=> false,
			'exclude_from_search' 	=> false,
			'query_var'           	=> true,
			'capability_type'     	=> 'post',
			'menu_position'			=> 3,
			'rewrite'				=> array(
				'slug'				=> 'books'
			),
			'menu_icon'           	=> 'dashicons-book-alt'
		);
		register_post_type( 'book', $post_args );
	}

	public function custom_taxonomy(){
		$taxonomy_labels = array(
			'name'                        => _x('Institutions categories', 'adp'),
			'singular_name'               => _x('Institution category', 'adp'),
		);

		$taxonomy_rewrite = array(
			'slug'                  => 'institution-category',
			'with_front'            => true,
			'hierarchical'          => true,
		);

		$taxonomy_args = array(
			'labels'              => $taxonomy_labels,
			'hierarchical'        => true,
			'public'              => true,
			'show_ui'             => true,
			'show_admin_column'   => true,
			'show_in_nav_menus'   => true,
			'show_tagcloud'       => true,
			'publicly_queryable'  => true,
			'rewrite'             => $taxonomy_rewrite,
		);
		register_taxonomy( 'institution-category', 'institution', $taxonomy_args );

		$taxonomy_labels = array(
			'name'                        => _x('Books categories', 'adp'),
			'singular_name'               => _x('Book category', 'adp'),
		);

		$taxonomy_rewrite = array(
			'slug'                  => 'book-category',
			'with_front'            => true,
			'hierarchical'          => true,
		);

		$taxonomy_args = array(
			'labels'              => $taxonomy_labels,
			'hierarchical'        => true,
			'public'              => true,
			'show_ui'             => true,
			'show_admin_column'   => true,
			'show_in_nav_menus'   => true,
			'show_tagcloud'       => true,
			'publicly_queryable'  => false,
			'rewrite'             => $taxonomy_rewrite,
		);
		register_taxonomy( 'book-category', 'book', $taxonomy_args );
	}

	public function limit_text( $text, $limit ) {
	    if (str_word_count($text, 0) > $limit) {
	        $words = str_word_count($text, 2);
	        $pos   = array_keys($words);
	        $text  = substr($text, 0, $pos[$limit]) . '...';
	    }
	    return $text;
	}
}

$theme_settings = new ThemeSettingsClass();