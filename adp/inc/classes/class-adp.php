<?php

class AdPHandbuchClass {
	public function __construct(){
		$this->actions_init();
	}

	public function actions_init(){
		add_action( 'init', array( $this, 'customer_role') );
		add_action( 'init', array( $this, 'hide_admin_bar') );
		// add_action( 'init', array( $this, 'check_authorization') );
	}

	public function customer_role(){
		$roles = $GLOBALS['wp_roles']->is_role('customer');
		if( $roles ) return false;

		add_role('customer', __('Customer', 'adp'), array(
			'read'			=> true,
			'edit_posts'	=> false,
			'upload_files'	=> false
		));
	}

	public function hide_admin_bar(){
		$user = wp_get_current_user();

		if( $user ) {
			if( in_array( 'customer', (array) $user->roles ) ){
				add_filter('show_admin_bar', '__return_false');
			}
		}
	}

	public function check_authorization(){
		if( is_user_logged_in() ){
			return true;
		} else {
			return false;
		}
	}
}

$adp = new AdPHandbuchClass();