<?php
/**
 *
 * @package WordPress
 * @subpackage ADP
 * @since 1.0
 * @version 1.0
 */
$privacy = get_privacy_policy_url(); 
?>
    </main>
    <footer class="adp-footer">
    	<div class="top__nav">
    		<div class="container">
	    		<div class="row">
	    			<div class="col-6">
	            	<?php if ( is_active_sidebar( 'footer-1' ) ) { ?>
	                	<div class="adp-footer__information">
	                	<?php dynamic_sidebar( 'footer-1' ); ?>
	                	</div>
	                <?php } 
	                if ( is_active_sidebar( 'footer-2' ) ) { ?>
	                	<div class="adp-footer__horizontal"><?php dynamic_sidebar( 'footer-2' ); ?></div>
	                <?php } ?>
					</div>
					<?php if ( is_active_sidebar( 'footer-3' ) ) { ?>
						<div class="col-2">
							<?php dynamic_sidebar( 'footer-3' ); ?>
						</div>
					<?php } ?>
					<?php if ( is_active_sidebar( 'footer-4' ) ) { ?>
						<div class="col-2">
							<?php dynamic_sidebar( 'footer-4' ); ?>
						</div>
					<?php } ?>
					<?php if ( is_active_sidebar( 'footer-5' ) ) { ?>
						<div class="col-2">
							<?php dynamic_sidebar( 'footer-5' ); ?>
						</div>
					<?php } ?>
				</div>
			</div>
    	</div>
	    <div class="bottom__nav">
	    	<div class="container">
				<div class="row">
					<div class="col">
						<div class="adp-copyright__block">
							<?php the_field('copyright_text', 'option'); ?>
						</div>
					</div>
				</div>
    		</div>
    	</div>
    </footer>
    <?php wp_footer(); ?>
</body>
</html>