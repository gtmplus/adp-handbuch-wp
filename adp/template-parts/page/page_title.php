<section class="adp-page__title">
	<div class="container">
		<div class="row">
			<div class="col">
				<div class="page__title">
					<h1 class="h2"><b><?php the_title(); ?></b></h1>
				</div>
			</div>
		</div>
	</div>
</section>