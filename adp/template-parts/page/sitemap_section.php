<?php 
$sitemap = get_sub_field('sitemap');
?>
<section class="adp-sitemap__section">
	<div class="container">
		<div class="row justify-content-end">
			<div class="col-8">
				<?php if( $sitemap ) { ?>
					<div class="adp-sitemap__block">
						<div class="row">
						<?php foreach ( $sitemap as $column ) { ?>
							<div class="col-6">
								<div class="adp-sitemap__column">
									<?php if( $column['title_label'] ) { ?>
									<div class="title">
										<?php if( $column['title_link'] ) { ?>
										<a href="<?php echo $column['title_link']; ?>">
											<h2 class="h1"><?php echo $column['title_label']; ?></h2>
										</a>
										<?php } else { ?>
										<h2 class="h1"><?php echo $column['title_label']; ?></h2>
										<?php } ?>
									</div>
									<?php } 
									if( $column['links'] ) { ?>
									<div class="links__block">
										<ul>
										<?php foreach ( $column['links'] as $link ) { 
										if( $link['link'] ) { ?>
											<li>
												<a href="<?php echo $link['link']; ?>"><?php echo $link['label']; ?></a>
											</li>
										<?php } 
										} ?>
										<ul>
									</div>
									<?php } ?>
								</div>
							</div>
						<?php } ?>
						</div>
					</div>
				<?php } ?>
			</div>
		</div>
	</div>
</section>