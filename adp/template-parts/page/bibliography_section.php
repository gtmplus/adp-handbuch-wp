<?php
$categories = get_sub_field('choose_categories_to_show');
?>
<section class="adp-bibliography__section">
	<div class="container">
		<div class="row">
			<div class="col">
				<?php foreach ( $categories as $cat ) { ?>
				<div class="category__block">
					<h3><b><?php echo $cat->name; ?></b></h3>
					<?php
					$args = array(
						'posts_per_page'	=> -1,
						'post_type'			=> 'book',
						'orderby'			=> 'title',
						'order'				=> 'ASC',
						'tax_query' => array(
							array(
								'taxonomy'	=> 'book-category',
								'field'		=> 'term_id',
								'terms'		=> $cat->term_id
							)
						)
					);
					$query = new WP_Query($args);

					if ( $query->have_posts() ) { ?>
					<div class="adp-bibliography__table">
						<div class="thead">
							<div class="tr">
								<div class="td book"><?php _e('Book', 'adp'); ?></div>
								<div class="td author"><?php _e('Author', 'adp'); ?></div>
							</div>
						</div>
						<div class="tbody">
							<?php while ( $query->have_posts() ) { $query->the_post(); 
								$authors = get_field('authors'); ?>
							<div class="tr">
								<div class="td book">
									<h4><b><?php the_title(); ?></b></h4>
									<?php the_field('description'); ?>
								</div>
								<div class="td author">
									<?php if( $authors ) { 
										$i = 1;
										$size = sizeof($authors);
										foreach ( $authors as $author ) { ?>
										 	<a href="<?php echo get_the_permalink( $author ); ?>">
										 		<b>
										 			<?php echo get_the_title( $author ); 
										 			if( $size > 1 && $i < $size ) echo ', '; ?>	
										 		</b>
										 	</a>
										<?php $i++; }
									} else { ?>–<?php } ?>
								</div>
							</div>
							<?php } ?>
						</div>
					</div>
					<?php } wp_reset_postdata(); ?>
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
</section>