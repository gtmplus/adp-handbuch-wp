<?php
$boards = get_sub_field('scientific_advisory_board');
?>
<section class="adp-scientific__board__section">
	<div class="container">
		<div class="row">
			<div class="col">
				<?php if( get_sub_field('title') ) { ?>
				<div class="title">
					<h3><b><?php the_sub_field('title'); ?></b></h3>
				</div>
				<?php } 
				if( $boards ) { ?>
				<table class="adp-scientific__board tablesorter">
					<thead class="thead">
						<tr class="tr">
							<th class="td name"><?php _e('Name', 'adp'); ?></th>
							<th class="td author"><?php _e('Area', 'adp'); ?></th>
							<th class="td address"><?php _e('Address', 'adp'); ?></th>
						</tr>
					</thead>
					<tbody class="tbody">
					<?php foreach ( $boards as $row ) { ?>
						<tr class="tr">
							<td class="td name">
								<?php if( $row['name'] ) { ?><h4><b><?php echo $row['name']; ?></b></h4><?php } ?>
								<?php if( $row['position'] ) { ?><p><?php echo $row['position']; ?></p><?php } ?>
							</td>
							<td class="td author">
								<?php if( $row['area'] ) { ?><p><?php echo $row['area']; ?></p><?php } ?>
							</td>
							<td class="td address"><?php echo $row['address']; ?></td>
						</tr>
					<?php } ?>
					</tbody>
				</table>
				<?php } ?>
			</div>
		</div>
	</div>
</section>