<section class="adp-content__editor">
	<div class="container">
		<div class="row">
			<div class="col">
				<div class="text"><?php the_sub_field('text'); ?></div>
			</div>
		</div>
	</div>
</section>