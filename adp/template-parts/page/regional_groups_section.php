<?php
$groups = get_sub_field('regional_groups');
?>
<section class="adp-regional__section">
	<div class="container">
		<div class="row">
			<?php if( get_sub_field('text') ) { ?>
			<div class="col-md-8">
				<div class="text"><?php the_sub_field('text'); ?></div>
			</div>
			<div class="col-md-1"></div>
			<?php }
			if( get_sub_field('image') ) { ?>
			<div class="col-md-3">
				<div class="image">
					<img src="<?php echo get_sub_field('image')['url']; ?>" alt="<?php echo get_sub_field('image')['title']; ?>">
				</div>
			</div>
			<?php } ?>
		</div>
		<?php 
		$args = array(
			'post_type'		=> 'regional-group',
			'order'			=> 'ASC',
			'orderby'		=> 'meta_value',
			'meta_key'		=> 'zip_code_short',
			'posts_per_page'=> -1
		);
		$query = new WP_Query( $args );
		if ( $query->have_posts() ) { ?>
		<div class="row">
			<div class="col">
				<div class="adp-regional__table">
					<div class="thead">
						<div class="tr">
							<div class="td zip"><?php _e('ZIP code area', 'adp'); ?></div>
							<div class="td city"><?php _e('City', 'adp'); ?></div>
							<div class="td person"><?php _e('Contact person', 'adp'); ?></div>
						</div>
					</div>
					<div class="tbody">
					<?php while ( $query->have_posts() ) { $query->the_post(); 
						$id = get_the_ID(); ?>
						<div class="tr">
							<div class="td zip"><?php echo get_field('zip_code_short', $id); ?></div>
							<div class="td city">
								<h4><b><?php echo get_the_title($id); ?></b></h4>
								<p><?php echo get_field('zip_code_area', $id); ?></p>
							</div>
							<div class="td person">
								<?php
								$persons = get_field('contact_person', $id);
								if( $persons ){
									foreach( $persons as $person ) { ?>
										<div class="person__details">
											<a href="<?php echo get_the_permalink( $person['person'] ); ?>">
												<?php echo get_the_title( $person['person'] ); ?>
											</a>
											<?php if( $person['about_person'] ) { ?>
												<div class="text"><?php echo $person['about_person']; ?></div>
											<?php } ?>
											<div class="address">
												<p>
													<?php 
													if( get_field( 'address', $person['person'] ) ) {
														echo __('Adresse: ', 'adp').get_field('address', $person['person'] );
													} 
													if( get_field( 'phone', $person['person'] ) ) {
														if( get_field( 'address', $person['person'] ) ) echo ', '; 
														echo __('Phone: ', 'adp').get_field('phone', $person['person'] ); 
													} 
													if( get_field( 'fax', $person['person'] ) ) {
														if( get_field( 'address', $person['person'] ) || get_field( 'address', $person['person'] ) ) echo ', ';
														echo __('Fax: ', 'adp').get_field('fax', $person['person'] ); 
													}
													if( get_field( 'email', $person['person'] ) ) {
														if( get_field( 'address', $person['person'] ) || get_field( 'address', $person['person'] ) || get_field( 'fax', $person['person'] ) ) echo ', ';
														echo __('E-Mail: ', 'adp').'<a href="mailto:'.get_field('email', $person['person'] ).'">'.get_field('email', $person['person'] ).'</a>'; 
													} ?>		
												</p>
											</div>
										</div>
									<?php } 
								} ?>
							</div>
						</div>
					<?php } ?>
					</div>
				</div>	
			</div>
		</div>
		<?php } wp_reset_postdata(); ?>
	</div>
</section>