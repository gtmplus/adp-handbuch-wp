<section class="adp-authorization__section">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-6">
				<div class="adp-form__block">
					<div class="title">
						<h2><b><?php _e('You must be logged in to view site content.', 'adp'); ?></b></h2>
					</div>
					<?php wp_login_form(); ?>
				</div>
			</div>
		</div>
	</div>
</section>