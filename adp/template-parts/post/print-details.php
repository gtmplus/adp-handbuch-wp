<?php 
$details = get_field('details_for_print');
$published = get_field('published_date') ? get_field('published_date') : get_the_modified_date('d.m.Y');
$modified = get_field('last_update') ? get_field('last_update') : get_the_modified_date('d.m.Y');
?>
<div class="adp-print__details">
	<div class="title"><h2><?php _e('Arbeitskreis der Pankreatektomierten e.V.', 'adp'); ?></h2></div>
	<div class="details__block">
		<div class="column">
			<div class="tr">
				<div class="td title"><?php _e('Version:', 'adp'); ?></div>
				<div class="td"><?php echo $details['article_version']; ?></div>
			</div>
			<div class="tr">
				<div class="td title"><?php _e('Section:', 'adp'); ?></div>
				<div class="td"><?php echo $details['article_section']; ?></div>
			</div>
			<div class="tr">
				<div class="td title"><?php _e('Running number:', 'adp'); ?></div>
				<div class="td"><?php echo $details['running_number']; ?></div>
			</div>
		</div>
		<div class="column">
			<div class="tr">
				<div class="td title"><?php _e('Published date:', 'adp'); ?></div>
				<div class="td"><?php echo $published; ?></div>
			</div>
			<div class="tr">
				<div class="td title"><?php _e('Last update:', 'adp'); ?></div>
				<div class="td"><?php echo $modified; ?></div>
			</div>
			<div class="tr">
				<div class="td title"><?php _e('Replace version:', 'adp'); ?></div>
				<div class="td"><?php echo $details['replace_version']; ?></div>
			</div>
		</div>
	</div>
</div>