<?php 
global $wp_query;
$total = $wp_query->max_num_pages;

if ( $total > 1 )  { ?>
    <div class="row">
    	<div class="col">
    		<div class="adp-posts__pagination">
			    <?php if ( !$current_page = get_query_var('paged') ) $current_page = 1;
			    
			    $format = '?paged=%#%';
			    echo paginate_links(array(
			        'base' 		=> @add_query_arg('paged','%#%'),
			        'format' 	=> $format,
			        'current' 	=> $current_page,
			        'total' 	=> $total,
			        'mid_size' 	=> 4,
			        'prev_text'	=> '<span class="adp-prev__page"></span>',
			        'next_text'	=> '<span class="adp-next__page"></span>',
			        'type' 		=> 'list'
			    )); ?>
    		</div>
    	</div>
	</div>
<?php }