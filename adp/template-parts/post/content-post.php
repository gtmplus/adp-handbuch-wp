<?php
$blog_page = get_option('page_for_posts');
$categories = get_the_category(get_the_ID());
$details = get_field('details_for_print');
$published = get_field('published_date') ? get_field('published_date') : get_the_modified_date('d.m.Y');
$modified = get_field('last_update') ? get_field('last_update') : get_the_modified_date('d.m.Y');
?>
<section class="adp-post__content">
	<div class="container">
		<div class="row">
			<div class="col">
				<div class="page__title">
					<h1 class="h2"><b><?php the_title(); ?></b></h1>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-print-12 col-9">
				<table class="content__section">
					<thead class="header">
						<tr>
							<td>
								<?php 
			                    $logo = get_field('logo', 'option'); 
			                    $text = get_field('logo_line', 'option');
			                    if( $logo ) { ?>
			                    <div class="adp-print__logo text-center float-left">
			                        
			                        <img src="<?php echo $logo['url']; ?>" alt="<?php echo get_bloginfo('name'); ?>">
			                        <?php } 
			                        if( $text ) { ?>
			                        <h4><?php echo $text; ?></h4>
			                        
			                    </div>
			                    <?php } ?>
								<div class="adp-print__details float-left">
									<div class="title">
										<h2><?php _e('Arbeitskreis der Pankreatektomierten e.V.', 'adp'); ?></h2>
									</div>
									<div class="details__block">
										<?php if( $details['article_version'] || $details['article_section'] || $details['running_number'] ) { ?>
										<div class="column">
											<?php if( isset($details['article_version']) ) { ?>
											<div class="tr">
												<div class="td title"><?php _e('Version:', 'adp'); ?></div>
												<div class="td"><?php echo $details['article_version']; ?></div>
											</div>
											<?php } 
											if( isset($details['article_section'] ) ) { ?>
											<div class="tr">
												<div class="td title"><?php _e('Section:', 'adp'); ?></div>
												<div class="td"><?php echo $details['article_section']; ?></div>
											</div>
											<?php } 
											if( isset($details['running_number'] ) ) { ?>
											<div class="tr">
												<div class="td title"><?php _e('Running number:', 'adp'); ?></div>
												<div class="td running"><?php echo $details['running_number']; ?><span></span></div>
											</div>
											<?php } ?>
										</div>
										<?php } ?>
										<div class="column">
											<?php if( $published ) { ?>
											<div class="tr">
												<div class="td title"><?php _e('Published date:', 'adp'); ?></div>
												<div class="td"><?php echo $published; ?></div>
											</div>
											<?php } 
											if( $modified ) { ?>
											<div class="tr">
												<div class="td title"><?php _e('Last update:', 'adp'); ?></div>
												<div class="td"><?php echo $modified; ?></div>
											</div>
											<?php } 
											if( $details['replace_version'] ) { ?>
											<div class="tr">
												<div class="td title"><?php _e('Replace version:', 'adp'); ?></div>
												<div class="td"><?php echo $details['replace_version']; ?></div>
											</div>
											<?php } ?>
										</div>
									</div>
								</div>
							</td>
						</tr>
					</thead>
					<tbody class="body">
						<tr>
							<td>
								<div class="title__line">
									<?php 
									if ( function_exists('yoast_breadcrumb') ) yoast_breadcrumb( '<div class="adp-breadcrumbs">','</div>' ); ?>
									<h1 class="h2"><b><?php the_title(); ?></b></h1>
								</div>
								<div class="content__wrapper"><?php the_content(); ?></div>
								<div class="footer">
									<?php $author = get_field('author');
									if( $author ) { ?>
										<h5><b><?php _e('Author', 'adp'); ?></b></h5>
										<p>
											<?php
											if( get_the_title( $author ) ) echo get_the_title( $author ); 
											if( get_field( 'name_institution', $author ) ) echo ', '.strip_tags(get_field( 'name_institution', $author ));
											if( get_field( 'address', $author ) ) echo ', '.strip_tags(get_field( 'address', $author ));
											if( get_field( 'phone', $author ) ) echo ', '.strip_tags(get_field( 'phone', $author ));
											if( get_field( 'fax', $author ) ) echo ', '.strip_tags(get_field( 'fax', $author ));
											if( get_field( 'email', $author ) ) echo ', E-Mail:'.strip_tags(get_field( 'email', $author ));
											?>
										</p>
									<?php } ?>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="col-3">
				<div class="sidebar">
					<?php if( get_field('author') ) { ?>
					<div class="author">
						<h4><b><?php _e('Author', 'adp'); ?></b></h4>
						<a href="<?php echo get_the_permalink( get_field('author') ); ?>"><?php echo get_the_title( get_field('author') ); ?></a>
					</div>
					<?php } ?>
					<div class="original">
						<h4><b><?php _e('Original post', 'adp'); ?></b></h4>
						<p><?php echo $published; ?></p>
					</div>
					<div class="update">
						<h4><b><?php _e('Last update', 'adp'); ?></b></h4>
						<p><?php echo $modified; ?></p>
					</div>
					<div class="additional">
						<span class="adp-print__post"><?php _e('Print Page', 'adp'); ?></span>
						<?php if( $blog_page ) { ?>
							<a href="<?php echo get_the_permalink( $blog_page ); ?>"><?php _e('Back to overview', 'adp'); ?></a>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
		<?php if( $categories ) { ?>
		<div class="row">
			<div class="col">
				<div class="adp-post__navigation">
					<a href="<?php echo get_category_link( $categories[0]->term_id ); ?>">
						<h3><b><?php _e('Back to overview', 'adp'); ?></b></h3>
					</a>
					<h5><?php echo $categories[0]->name; ?></h5>
				</div>
			</div>
		</div>
		<?php } ?>
	</div>
</section>