<?php 
$theme_settings = new ThemeSettingsClass();
$title = $theme_settings->limit_text(get_the_title(), 6);
$excerpt = $theme_settings->limit_text( get_the_excerpt(), 24 );
?>
<div class="col-4">
	<a href="<?php the_permalink(); ?>" class="adp-posts__block">
		<div class="content">
			<h3><b><?php echo $title; ?></b></h3>
			<p><?php echo $excerpt; ?></p>
		</div>
		<span class="read__more"><?php _e('Read more', 'adp'); ?></span>
	</a>
</div>