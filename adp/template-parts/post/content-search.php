<div class="col-12">
	<div class="adp-search__result">
		<a href="<?php the_permalink(); ?>"><h3><b><?php the_title(); ?></b></h3></a>
		<?php the_excerpt(); ?>
		<a href="<?php the_permalink(); ?>" class="read__more"><b><?php _e('Read more', 'adp'); ?></b></a>
	</div>
</div>