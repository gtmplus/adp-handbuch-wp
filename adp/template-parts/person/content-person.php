<section class="adp-person__section">
	<div class="container">
		<div class="row">
			<div class="col">
				<div class="adp-title">
					<h1 class="h2"><b><?php the_title(); ?></b></h1>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="adp-information__block">
					<div class="block__title">
						<h3><b><?php _e('General information', 'adp'); ?></b></h3>
					</div>
					<?php if( get_field('address') ) { ?>
					<div class="tr">
						<div class="td title"><?php _e('Address:', 'adp'); ?></div>
						<div class="td"><p><?php the_field('address'); ?></p></div>
					</div>
					<?php } 
					if( get_field('phone') ) { ?>
					<div class="tr">
						<div class="td title"><?php _e('Phone:', 'adp'); ?></div>
						<div class="td"><p><?php the_field('phone'); ?></p></div>
					</div>
					<?php } 
					if( get_field('fax') ) { ?>
					<div class="tr">
						<div class="td title"><?php _e('Fax:', 'adp'); ?></div>
						<div class="td"><p><?php the_field('fax'); ?></p></div>
					</div>
					<?php } 
					if( get_field('email') ) { ?>
					<div class="tr">
						<div class="td title"><?php _e('E-Mail:', 'adp'); ?></div>
						<div class="td"><a href="mailto:<?php the_field('email'); ?>"><b><?php the_field('email'); ?></b></a></div>
					</div>
					<?php } ?>
				</div>
				<?php 
				$id = get_the_ID();
				$args = array(
					'post_type'			=> 'post',
					'posts_per_page'	=> -1,
					'orderby'			=> 'modified',
					'meta_query'		=> array(
						array(
							'key'	=> 'author',
							'value' => $id
						)
					)
				);
				$query = new WP_Query( $args );

				if ( $query->have_posts() ) { ?>
				<div class="adp-information__block">
					<div class="block__title">
						<h3><b><?php _e('Articles written', 'adp'); ?></b></h3>
					</div>
					<ul>
						<?php while ( $query->have_posts() ) { $query->the_post(); ?>
							<li>
								<a href="<?php the_permalink(); ?>"><b><?php the_title(); ?></b></a>
							</li>
						<?php } ?>
					</ul>
				</div>
				<?php } wp_reset_postdata();
				$args = array(
					'post_type'			=> 'book',
					'posts_per_page'	=> -1,
					'orderby'			=> 'modified',
					'meta_query'		=> array(
						array(
							'key'		=> 'authors',
							'value' 	=> $id,
							'compare'	=> 'LIKE' 
						)
					)
				);
				$query = new WP_Query( $args );
				$biblioraphy_page = get_field('bibliography_page', 'option');
				if ( $query->have_posts() ) { ?>
				<div class="adp-information__block">
					<div class="block__title">
						<h3><b><?php _e('Bibliography', 'adp'); ?></b></h3>
					</div>
					<ul>
						<?php while ( $query->have_posts() ) { $query->the_post(); ?>
							<li>
								<a href="<?php echo get_permalink( $biblioraphy_page ); ?>"><b><?php the_title(); ?></b></a>
							</li>
						<?php } ?>
					</ul>
				</div>
				<?php } wp_reset_postdata(); ?>
			</div>
			<div class="col-md-1"></div>
			<div class="col-md-5">
				<?php 
				$api_key = get_field('google_map_api_key', 'option');
				if( get_field('address') && $api_key ) { ?>
					<div class="adp-google__map">
						<iframe width="600"	height="450" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?key=<?php echo $api_key; ?>&q=<?php echo strip_tags(get_field('address')); ?>" allowfullscreen></iframe>
					</div>
				<?php } 
				if( get_field('name_institution') ) { ?>
				<div class="adp-person__content">
					<h3><b><?php _e('Name of a company/clinic or institute', 'adp'); ?></b></h3>
					<p><?php the_field('name_institution'); ?></p>
				</div>
				<?php }
				if( get_field('content') ) { ?>
				<div class="adp-person__content">
					<h3><b><?php _e('Biography', 'adp'); ?></b></h3>
					<?php the_field('content'); ?>		
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
</section>