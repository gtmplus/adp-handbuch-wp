<section class="adp-institution__section">
	<div class="container">
		<div class="row">
			<div class="col">
				<div class="adp-title">
					<h1 class="h2"><b><?php the_title(); ?></b></h1>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="adp-information__block institution">
					<?php if( get_field('address') ) { ?>
					<div class="tr">
						<div class="td title"><?php _e('Address:', 'adp'); ?></div>
						<div class="td"><?php the_field('address'); ?></div>
					</div>
					<?php } 
					if( get_field('phone') ) { ?>
					<div class="tr">
						<div class="td title"><?php _e('Phone:', 'adp'); ?></div>
						<div class="td"><?php the_field('phone'); ?></div>
					</div>
					<?php } 
					if( get_field('fax') ) { ?>
					<div class="tr">
						<div class="td title"><?php _e('Fax:', 'adp'); ?></div>
						<div class="td"><?php the_field('fax'); ?></div>
					</div>
					<?php } 
					if( get_field('email') ) { ?>
					<div class="tr">
						<div class="td title"><?php _e('E-Mail:', 'adp'); ?></div>
						<div class="td"><a href="mailto:<?php the_field('email'); ?>"><b><?php the_field('email'); ?></b></a></div>
					</div>
					<?php } 
					if( get_field('web_url') ) { ?>
					<div class="tr">
						<div class="td title"><?php _e('Web url:', 'adp'); ?></div>
						<div class="td"><a href="<?php the_field('web_url'); ?>"><b><?php the_field('web_url'); ?></b></a></div>
					</div>
					<?php } 
					if( get_field('holder') ) { ?>
					<div class="tr margin__top">
						<div class="td title"><?php _e('Holder:', 'adp'); ?></div>
						<div class="td"><?php the_field('holder'); ?></div>
					</div>
					<?php } 
					$ids = get_field('contact_person');
					if( $ids ) { 
						foreach ( $ids as $id ) { ?>
							<div class="tr">
								<div class="td title"><?php _e('Contact person:', 'adp'); ?></div>
								<div class="td">
									<a href="<?php echo get_the_permalink( $id['contact_person'] ); ?>"><b><?php echo get_the_title( $id['contact_person'] ); ?></b></a>
								</div>
							</div>
							<?php if( $id['about_person'] ) { ?>
							<div class="tr">
								<div class="td title"><?php _e('Subject area:', 'adp'); ?></div>
								<div class="td">
									<?php echo $id['about_person']; ?>
								</div>
							</div>
							<?php }
						} 
					} ?>
				</div>
			</div>
			<div class="col-md-1"></div>
			<div class="col-md-5">
				<?php 
				$api_key = get_field('google_map_api_key', 'option');
				if( get_field('address') && $api_key ) { ?>
					<div class="adp-google__map">
						<iframe width="600"	height="450" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?key=<?php echo $api_key; ?>&q=<?php echo strip_tags(get_field('address')); ?>" allowfullscreen></iframe>
					</div>
				<?php } ?>
			</div>
		</div>
		<div class="row">
			<div class="col">
				<div class="adp-post__navigation">
					<a href="<?php echo get_post_type_archive_link( 'institution' ); ?>">
						<h3><b><?php _e('Back to overview', 'adp'); ?></b></h3>
					</a>
					<?php 
					$institution = get_field('institution', 'option');
					if( $institution['title'] ) { ?>
					<h5><?php _e('Rehabilitation clinics for pancreas surgery', 'adp'); ?></h5>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</section>