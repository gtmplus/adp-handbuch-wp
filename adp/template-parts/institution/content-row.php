<?php 
$ids = get_field('contact_person'); 
?>
<div class="tr">
	<div class="td name">
		<a href="<?php the_permalink(); ?>"><b><?php the_title(); ?></b></a>
	</div>
	<div class="td details">
		<?php if( $ids ) { 
			foreach ( $ids as $id ) { ?>
			<a href="<?php echo get_the_permalink( $id['contact_person'] ); ?>"><b><?php echo get_the_title( $id['contact_person'] ); ?></b></a>
			<?php the_field('content', $id['contact_person']); ?>
			<?php } 
		} ?>
	</div>
	<div class="td address">
		<?php the_field('address'); ?>
		<?php if( get_field('phone') ) { ?><p><?php _e('Phone: ', 'adp'); the_field('phone'); ?></p><?php } ?>
	</div>
</div>