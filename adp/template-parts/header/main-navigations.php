<header class="adp-header">
    <div class="top__line">
        <div class="container">
            <div class="row">
                <div class="col">
                    <?php 
                    $logo = get_field('logo', 'option'); 
                    $text = get_field('logo_line', 'option'); ?>
                    <a class="adp-logo text-center float-left" href="<?php echo esc_url( home_url( '/' ) ); ?>">
                        <?php if( $logo ) { ?>
                        <img src="<?php echo $logo['url']; ?>" alt="<?php echo get_bloginfo('name'); ?>">
                        <?php } 
                        if( $text ) { ?>
                        <h4><?php echo $text; ?></h4>
                        <?php } ?>
                    </a>
                    
                    <div class="adp-nav__bar float-right">
                        <?php if( has_nav_menu('top') ) { ?>
                        <div class="adp-top__menu float-left">
                            <?php wp_nav_menu( array(
                                'theme_location'        => 'top',
                                'container'             => 'nav',
                                'container_class'       => 'adp-top__nav float-left'
                            ) ); 
                            ?>
                        </div>
                        <?php } ?>
                        <div class="adp-service__bar float-left">
                            <div class="scale" title="<?php _e('Scale', 'adp'); ?>"></div>
                            <?php if( is_user_logged_in() ) { 
                                $user_name = wp_get_current_user()->display_name; ?>
                                <a href="<?php echo get_edit_profile_url(); ?>" class="user" title="<?php _e('Hello ', 'adp'); echo $user_name; ?>"></a>
                            <?php } ?>
                            <div class="print" title="<?php _e('Print', 'adp'); ?>"></div>
                            <div class="search" title="<?php _e('Search', 'adp'); ?>"></div>
                            <div class="search__bar"><?php get_search_form(); ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bottom__line">
        <div class="container">
            <div class="row">
                <div class="col">
                    <?php if( has_nav_menu('bottom') ) { ?>
                    <div class="adp-bottom__menu float-right">
                        <?php wp_nav_menu( array(
                            'theme_location'        => 'bottom',
                            'container'             => 'nav',
                            'container_class'       => 'adp-bottom__nav float-right'
                        ) ); 
                        ?>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</header>