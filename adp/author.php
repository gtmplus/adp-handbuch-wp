<?php
/**
 *
 * @package WordPress
 * @subpackage ADP
 * @since 1.0
 * @version 1.0
 */
$adp = new AdPHandbuchClass();
get_header(); 
if( $adp->check_authorization() ){
	get_template_part( 'template-parts/page/breadcrumbs' );
	$page = get_query_var('paged') ? get_query_var('paged') : 1;
	$args = array(
		'post_status'	=> 'publish',
		'orderby'		=> 'modified',
		'paged'			=> $page,
		'author'		=> get_query_var( 'author' )
	); 
	$query = new WP_Query($args); 
	if ( $query->have_posts() ) { ?>
	<section class="adp-posts__section">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="page__title">
						<h1 class="h2"><b><?php _e('Author\'s publications: '); echo get_the_author(); ?></b></h1>
					</div>
				</div>
			</div>
			<div class="row">
				<?php while ( $query->have_posts() ) { $query->the_post(); 
					get_template_part( 'template-parts/post/content', 'thumbnail' );
				} ?>
			</div>
			<?php get_template_part( 'template-parts/post/pagination' ); ?>
		</div>
	</section>
	<?php } else { ?>
	<section class="adp-posts__section">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="page__title">
						<h1 class="h2"><b><?php _e('Nothing to show', 'adp'); ?></b></h1>
					</div>
				</div>
			</div>
		</div>
	</section>
	<?php } wp_reset_postdata();
} else {
	get_template_part( 'template-parts/page/authorization_section' );
}

get_footer();