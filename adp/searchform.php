<?php
/**
 *
 * @package WordPress
 * @subpackage ADP
 * @since 1.0
 * @version 1.0
 */
?>

<form role="search" method="get" class="adp-search__form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<div class="adp-search__group">
		<input type="search" class="adp-search__field" placeholder="<?php _e('Search...', 'adp'); ?>" value="<?php echo get_search_query(); ?>" name="s" />
		<button type="submit" class="adp-search__submit"></button>
	</div>
</form>
