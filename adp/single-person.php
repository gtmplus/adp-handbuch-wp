<?php
/**
 *
 * @package WordPress
 * @subpackage ADP
 * @since 1.0
 * @version 1.0
 */
$adp = new AdPHandbuchClass();
get_header(); 
if( $adp->check_authorization() ){
	if ( have_posts() ) {
		get_template_part( 'template-parts/page/breadcrumbs' );
		while ( have_posts() ) {
			the_post();
			get_template_part( 'template-parts/person/content', 'person' );
		}
	}
} else {
	get_template_part( 'template-parts/page/authorization_section' );
}

get_footer();