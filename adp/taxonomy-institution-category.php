<?php
/**
 *
 * @package WordPress
 * @subpackage ADP
 * @since 1.0
 * @version 1.0
 */

$adp = new AdPHandbuchClass();
get_header(); 

if( $adp->check_authorization() ){
	get_template_part( 'template-parts/page/breadcrumbs' );
	$term = get_queried_object(); ?>
	<section class="adp-archive__section">
		<div class="container">
			<?php 
			if( $term->name ) { ?>
			<div class="row">
				<div class="col">
					<div class="page__title">
						<h1 class="h2"><b><?php echo $term->name; ?></b></h1>
					</div>
				</div>	
			</div>
			<?php } 

			$args = array(
				'post_type'			=> 'institution',
				'orderby'			=> 'title',
				'posts_per_page'	=> -1,
				'order'				=> 'ASC',
				'tax_query'			=> array(
					array(
						'taxonomy'		=> 'institution-category',
						'field'			=> 'term_id',
						'terms'			=> $term->term_id
					)
				)
			);
			$query = new WP_Query($args); 

			if ( $query->have_posts() ) : ?>
			<div class="row">
				<div class="col">
					<div class="adp-institution__table">
						<div class="thead">
							<div class="tr">
								<div class="td name"><?php _e('Clinic', 'adp'); ?></div>
								<div class="td details"><?php _e('Contact Person', 'adp'); ?></div>
								<div class="td address"><?php _e('Address', 'adp'); ?></div>
							</div>
						</div>
						<div class="tbody">
							<?php while ( $query->have_posts() ) { $query->the_post(); 
								get_template_part( 'template-parts/institution/content', 'row' );
							} wp_reset_postdata(); ?>
						</div>
					</div>
				</div>
			</div>
			<?php endif; ?>
		</div>
	</section>
<?php } else {
	get_template_part( 'template-parts/page/authorization_section' );
}

get_footer();