<?php
/**
 *
 * @package WordPress
 * @subpackage ADP
 * @since 1.0
 * @version 1.0
 */

get_header();
if( $adp->check_authorization() ){
	get_template_part( 'template-parts/page/breadcrumbs' );
	?>
	<section class="adp-posts__section">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="page__title">
						<h1 class="h2">
							<?php if ( have_posts() ) { ?>
								<b><?php printf( __( 'Search Results for: %s', 'adp' ), '<span>'.get_search_query().'</span>'); ?></b>
							<?php } else { ?>
								<b><?php _e( 'Nothing Found', 'adp' ); ?></b>
							<?php } ?>
						</h1>
					</div>
				</div>
			</div>
			<?php if ( have_posts() ) { ?>
				<div class="row">
					<?php while ( have_posts() ) { the_post();
						get_template_part( 'template-parts/post/content', 'search' );
					} ?>
				</div>
				<?php get_template_part( 'template-parts/post/pagination' ); ?>
			<?php } ?>
		</div>
	</section>
<?php } else {
	get_template_part( 'template-parts/page/authorization_section' );
}
get_footer();