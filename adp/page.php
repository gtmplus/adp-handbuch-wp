<?php
/**
 *
 * @package WordPress
 * @subpackage ADP
 * @since 1.0
 * @version 1.0
 */

$adp = new AdPHandbuchClass();
get_header(); 

if( $adp->check_authorization() ){
	get_template_part( 'template-parts/page/breadcrumbs' );
	get_template_part( 'template-parts/page/page_title' );
	if ( have_posts() ) {
		while ( have_posts() ) { the_post(); ?>
			<section class="adp-page__content">
				<div class="container">
					<div class="row">
						<div class="col">
							<div class="content"><?php the_content(); ?></div>
						</div>
					</div>
				</div>
			</section>
		<?php }
	}
	if( have_rows('content') ):
		while ( have_rows('content') ) : the_row();
			if( get_row_layout() == 'sitemap_section' ):
				get_template_part( 'template-parts/page/sitemap_section' );
			elseif( get_row_layout() == 'content_editor' ):
				get_template_part( 'template-parts/page/content_editor' );
			elseif( get_row_layout() == 'regional_groups_section' ):
				get_template_part( 'template-parts/page/regional_groups_section' );
			elseif( get_row_layout() == 'bibliography_section' ):
				get_template_part( 'template-parts/page/bibliography_section' );
			elseif( get_row_layout() == 'scientific_advisory_board_section' ):
				get_template_part( 'template-parts/page/scientific_advisory_board_section' );
			endif;
		endwhile;
	endif;
} else {
	get_template_part( 'template-parts/page/authorization_section' );
}

get_footer();