'use strict';

(function($) { 
    $(document).ready(function() {
        AOS.init();
    });
})(jQuery);

class GeneralClass{
    constructor(){

        this.init();
    }
    init(){
        this.documentReady();

        this.windowScroll();
    }

    documentReady(){
        document.addEventListener("DOMContentLoaded", function(){
            adp.headerNav();
            adp.printBar();
            adp.searchBar();
            adp.checkScale();
            adp.scaleBar();
            (function($) {
                $('.tablesorter').tablesorter();
            })(jQuery);
        });
    }

    windowScroll(){
        window.addEventListener('scroll', function() {
            adp.headerNav();
        });
    }

    headerNav(){
        let header = document.querySelector('header');
        if( !header ) return false;

        let topScroll = window.scrollY;

        if( topScroll > 100 ){
            header.classList.add('scroll');
        } else {
            header.classList.remove('scroll');
        }
    }

    checkScale(){
        let body        = document.querySelector('body'),
            scale       = adp.getFromStorage(),
            scaleBtn    = document.querySelector('.adp-service__bar .scale');

        if( scale == 'true' ){
            body.classList.add('scale');
            scaleBtn.classList.add('active');
        }
    }

    getFromStorage(){
        return sessionStorage.getItem('scale');
    }

    setToStorage(value){
        sessionStorage.setItem('scale', value);
    }

    scaleBar(){
        let scaleBtn = document.querySelector('.adp-service__bar .scale'),
            body = document.querySelector('body');

        scaleBtn.addEventListener('click', function() {
            let value = adp.getFromStorage();
            
            if( value == 'true' ){
                adp.setToStorage('false');
                body.classList.remove('scale');
                scaleBtn.classList.remove('active');
            } else {
                adp.setToStorage('true');
                body.classList.add('scale');
                scaleBtn.classList.add('active');
            }
        });
    }

    printBar(){
        let printBtn = document.querySelector('.adp-service__bar .print'),
            printPostBtn = document.querySelector('.adp-print__post');

        printBtn.addEventListener('click', function() {
            window.print();
        });
        
        if( printPostBtn ){
            printPostBtn.addEventListener('click', function() {
                window.print();
            });
        }
    }

    searchBar(){
        let searchBtn = document.querySelector('.adp-service__bar .search'),
            searchBar = document.querySelector('.adp-service__bar .search__bar');

        searchBtn.addEventListener('click', function(e) {
            if( searchBtn.classList.contains('show') ) {
                searchBar.classList.remove('show');
                searchBtn.classList.remove('show');
            } else {
                searchBar.classList.add('show');
                searchBtn.classList.add('show');
            }
        });
    }
}

let adp = new GeneralClass();